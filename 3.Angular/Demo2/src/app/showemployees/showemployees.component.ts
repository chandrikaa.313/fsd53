// showemployees.component.ts

import {Pipe, Component } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrls: ['./showemployees.component.css']
})
export class ShowemployeesComponent {
  employees = [
    {
      empId: 1,
      empName: 'Chandrika',
      salary: 50000,
      gender: 'Female',
      doj: '2018-01-01',
      country: 'USA',
      emailId: 'john.doe@example.com',
      password: 123,
      deptId: 'IT'
    },
    {
      empId: 2,
      empName: 'Akhila',
      salary: 60000,
      gender: 'Female',
      doj: '2019-02-15',
      country: 'Canada',
      emailId: 'jane.smith@example.com',
      password: 123,
      deptId: 'HR'
    },

    {
      empId: 3,
      empName: 'Kreesthu',
      salary: 80000,
      gender: 'Male',
      doj: '2020-03-15',
      country: 'Germany',
      emailId: 'janu.smith@example.com',
      password: 123,
      deptId: 'HR'
    },


    {
      empId: 4,
      empName: 'Fahim',
      salary: 90000,
      gender: 'Male',
      doj: '2021-03-15',
      country: 'UK',
      emailId: 'jashu.smith@example.com',
      password: 123,
      deptId: 'HR'
    },



    {
      empId: 5,
      empName: 'Susan',
      salary: 70000,
      gender: 'Female',
      doj: '2017-03-15',
      country: 'Switzerland',
      emailId: 'jain.smith@example.com',
      password: 123,
      deptId: 'HR'
    },
  ];


}