import { Component } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent {
   // Array of product data
   products = [
    {
      prodId: 1,
      name: 'Product 1',
      description: 'Description 1',
      price: 19.99,
      imgsrc: 'path/to/image1.jpg'
    },
    {
      prodId: 2,
      name: 'Product 2',
      description: 'Description 2',
      price: 29.99,
      imgsrc: 'path/to/image2.jpg'
    },
    {
      prodId: 3,
      name: 'Product 3',
      description: 'Description 3',
      price: 39.99,
      imgsrc: 'path/to/image2.jpg'
    },
  ];
}
