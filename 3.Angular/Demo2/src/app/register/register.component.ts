import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {

  empId: string = '';
  empName: string = '';
  salary: string = '';
  gender: string = '';
  doj: string = '';
  country: string = '';
  emailId: string = '';
  password: string = '';
  deptId: string = '';

  onSubmit() {
    console.log('Registration details submitted:');
    console.log('Employee ID: ', this.empId);
    console.log('Employee Name: ', this.empName);
    console.log('Salary: ', this.salary);
    console.log('Gender: ', this.gender);
    console.log('Date of Joining: ', this.doj);
    console.log('Country: ', this.country);
    console.log('Email ID: ', this.emailId);
    console.log('Password: ', this.password);
    console.log('Department ID: ', this.deptId);
}
}