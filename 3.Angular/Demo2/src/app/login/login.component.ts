import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent implements OnInit {

  employees: any;
  emp: any;

  //DateFormat: MM-dd-YYYY

  //Implementing Dependency Injection for Routing Module
  constructor(private router: Router) {
    this.employees = [
      { empId: 101, empName: 'Harsha', salary: 1212.12, gender: 'Male', doj: '05-25-2018', country: "IND", emailId: 'harsha@gmail.com', password: '123' },
      { empId: 102, empName: 'Pasha', salary: 2323.23, gender: 'Male', doj: '06-26-2017', country: "USA", emailId: 'pasha@gmail.com', password: '123' },
      { empId: 103, empName: 'Indira', salary: 3434.34, gender: 'Female', doj: '07-27-2016', country: "CHI", emailId: 'indira@gmail.com', password: '123' },
      { empId: 104, empName: 'Vamshi', salary: 4545.45, gender: 'Male', doj: '08-28-2015', country: "JAP", emailId: 'vamshi@gmail.com', password: '123' },
      { empId: 105, empName: 'Krishna', salary: 5656.56, gender: 'Male', doj: '09-29-2014', country: "UK", emailId: 'krishna@gmail.com', password: '123' }
    ];
  }

  ngOnInit(): void {
  }

  loginSubmit(loginForm: any) {
    console.log(loginForm);
    this.emp = null;

    if (loginForm.emailId === 'HR' && loginForm.password === 'HR') {
      this.router.navigate(['showemps']);
    } else {

      this.employees.forEach((element: any) => {
        if (element.emailId === loginForm.emailId && element.password === loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        this.router.navigate(['products']);
      } else {
        alert("Invalid Credentials");
      }
      
    }
  }
}
