import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  standalone: true,
  imports: [],
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {
  id: number;
  name: string;
  age: number;

  address: any;
  hobby: any;



  constructor(){
    this.id = 672;
    this.name = 'Chandrika';
    this.age = 22;

    this.address = {streetNo:672 , city:'Hyd', state:'Telegana'};

    this.hobby = ['playing' , 'singing'];
  }
}
