<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.model.Employee"%>

<!-- Add the following line inorder to use the JSTL tags -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetEmpById</title>
</head>
<body>

	<c:import url="HRHomePage.jsp" />
	<center>

		<table border=2>
			<thead>
				<tr>
					<th>EmpId</th>
					<th>EmpName</th>
					<th>Salary</th>
					<th>Gender</th>
					<th>EmailId</th>
				</tr>
			</thead>

			<tr>
				<td> ${ emp.empId   } </td>
				<td> ${ emp.empName } </td>
				<td> ${ emp.salary  } </td>
				<td> ${ emp.gender  } </td>
				<td> ${ emp.emailId } </td>
			</tr>

		</table>
	</center>
</body>
</html>
