package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDAO;
import com.model.Employee;

@WebServlet("/GetAllEmployees")
public class GetAllEmployees extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		EmployeeDAO empDAO = new EmployeeDAO();
		List<Employee> empList = empDAO.getAllEmployees();
		
if (empList != null) {
			
			//Storing the ListOfEmployee into the request object
			request.setAttribute("empList", empList);
			
			RequestDispatcher rd = request.getRequestDispatcher("GetAllEmployees.jsp");
			rd.forward(request, response);
			
		} else {
			out.print("<center><h1 style='color:red;'>Record(s) Not Found!!!</h1></center>");
		
		
		}}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
