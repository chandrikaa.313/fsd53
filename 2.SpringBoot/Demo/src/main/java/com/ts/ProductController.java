package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class ProductController {

	//Dependency Injection for ProductDAO
	@Autowired
	ProductDAO prodDAO;
	
	@RequestMapping("getAllProducts")
	public List<Product> getAllProducts() {	
		return prodDAO.getAllProducts();
	}	
	
	
	@RequestMapping("getProductHC")
	public Product getProductHC() {		
		Product product = new Product();		
		product.setProductId(101);
		product.setProductName("Laptop");
		product.setPrice(45999.00);		
		return product;
	}
	
	@RequestMapping("getAllProductsHC")
	public List<Product> getAllProductsHC() {		
		List<Product> productList = new ArrayList<Product>();		
		Product product1 = new Product(101, "Laptop", 45999.00);
		Product product2 = new Product(102, "Mobile", 85999.00);
		Product product3 = new Product(103, "Pendrive", 599.00);		
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);		
		return productList;
	}
	@GetMapping("getProductById/{id}")
	public Product getProductById(@PathVariable("id") int productid) {	
	    return prodDAO.getProductById(productid);		
	}

	
	@GetMapping("getProductByName/{name}")
	public Product getProductByName(@PathVariable("name") String prodName) {	
	    return prodDAO.getProductByName(prodName);		
	}
	
	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {		
	    return prodDAO.addProduct(product);
	}	
	
	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {		
	    return prodDAO.updateProduct(product);
	}	

	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int productid) {	
	    prodDAO.deleteProductById(productid);
	    return "Product Deleted Successfully";
	}

}
