package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
	
	//@Entity - Creating the Table based on the Entity Class
	//@Id     - Primary Key
	//@GeneratedValue - Auto_Increment
	//@Column - Providing different name for the column in the table rather than the existing variable name
	
	@Id@GeneratedValue
	private int productId;
	
	@Column(name="prodName")
	private String productName;
	private double price;
	
	public Product() {
		super();
	}

	public Product(int productId, String productName, double price) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.price = price;
	}

	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price + "]";
	}
}