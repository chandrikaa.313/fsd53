package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDAO;
import com.model.Department;

@RestController
public class DepartmentController {

	@Autowired
	DepartmentDAO deptDAO;
	
	@GetMapping("getDepartments")
	public List<Department> getDepartments() {
		return deptDAO.getDepartments();
	}
	
	@GetMapping("getDepartmentById/{deptId}")
	public Department getDepartmentById(@PathVariable("deptId") int deptId) {
		return deptDAO.getDepartmentById(deptId);
	}
	
	@GetMapping("getDepartmentByName/{deptName}")
	public Department getDepartmentByName(@PathVariable("deptName") String deptName) {
		return deptDAO.getDepartmentByName(deptName);
	}
	
	@PostMapping("addDepartment")
	public Department addDepartment(@RequestBody Department dept) {
		return deptDAO.addDepartment(dept);
	}
	
	@PutMapping("updateDepartment")
	public Department updateDepartment(@RequestBody Department dept) {
		return deptDAO.updateDepartment(dept);
	}
	
	@DeleteMapping("deleteDepartmentById/{deptId}")
	public String deleteDepartmentById(@PathVariable("deptId") int deptId) {
		deptDAO.deleteDepartmentById(deptId);
		return "Department with DepartmentId: " + deptId + ", Deleted Successfully";
	}
	
	
	@RequestMapping("addDepartmentsHC")
	public String addDepartmentsHC() {
		
		Department dept1 = new Department(10, "IT", "Hyd");
		Department dept2 = new Department(20, "Sales", "Chennai");
		Department dept3 = new Department(30, "Accounts", "Pune");
		
		deptDAO.addDepartment(dept1);
		deptDAO.addDepartment(dept2);
		deptDAO.addDepartment(dept3);
		
		return "3 Department(s) IT(10), Sales(20) and Accounts(30) Added Successfully!!";
	}
	
}
