package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDAO;
import com.dao.EmployeeDAO;
import com.model.Employee;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeDAO empDao;
	
	@Autowired
	DepartmentDAO deptDao;
	
	
	@GetMapping("empLogin/{emailId}/{password}")
	public Employee empLogin(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
		return empDao.empLogin(emailId, password);
	}
	
	
	@GetMapping("getEmployees")
	public List<Employee> getEmployees() {
		return empDao.getEmployees();
	}
	
	@GetMapping("getEmployeeById/{empId}")
	public Employee getEmployeeById(@PathVariable("empId") int empId) {
		return empDao.getEmployeeById(empId);
	}
	
	@GetMapping("getEmployeeByName/{empName}")
	public Employee getEmployeeByName(@PathVariable("empName") String empName) {
		return empDao.getEmployeeByName(empName);
	}
	
	@PostMapping("addEmployee")
	public Employee addEmployee(@RequestBody Employee emp) {
		return empDao.addEmploee(emp);
	}

	@PutMapping("updateEmployee")
	public Employee updateEmployee(@RequestBody Employee emp) {
		return empDao.updateEmployee(emp);
	}
	
	@DeleteMapping("deleteEmployeeById/{empId}")
	public String deleteEmployeeById(@PathVariable("empId") int empId) {
		empDao.deleteEmployeeById(empId);
		return "Employee with empId: " + empId + ", Deleted Successfully";
	}
	
	
	@RequestMapping("addEmployeesHC")
	public String addDepartmentsHC() {
		
		Employee emp1 = new Employee(101, "Harsha", 1212.12, "Male", null, "harsha@gmail.com", "123");
		emp1.setDepartment(deptDao.getDepartmentById(10));
		
		Employee emp2 = new Employee(102, "Pasha", 2121.21, "Male", null, "pasha@gmail.com", "123");
		emp2.setDepartment(deptDao.getDepartmentById(20));
		
		Employee emp3 = new Employee(103, "Indira", 2323.23, "Female", null, "indira@gmail.com", "123");
		emp3.setDepartment(deptDao.getDepartmentById(30));
		
		empDao.addEmploee(emp1);
		empDao.addEmploee(emp2);
		empDao.addEmploee(emp3);
				
		return "3 Employees(s) Harsha, Pasha and Indira Added Successfully!!";
	}
	
}
