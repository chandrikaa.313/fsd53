package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Department;

@Service
public class DepartmentDAO {

	@Autowired
	DepartmentRepository deptRepo;

	public List<Department> getDepartments() {
		return deptRepo.findAll();
	}

	public Department getDepartmentById(int deptId) {
		return deptRepo.findById(deptId).orElse(null);
	}

	public Department getDepartmentByName(String deptName) {
		return deptRepo.findByName(deptName);
	}
		
	public Department addDepartment(Department dept) {
		return deptRepo.save(dept);
	}

	public Department updateDepartment(Department dept) {
		return deptRepo.save(dept);
	}

	public void deleteDepartmentById(int deptId) {
		deptRepo.deleteById(deptId);
	}
	
}
