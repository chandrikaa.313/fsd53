package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDAO {

	@Autowired
	EmployeeRepository empRepo;

	public List<Employee> getEmployees() {
		return empRepo.findAll();
	}
	
	public Employee getEmployeeById(int empId) {
		return empRepo.findById(empId).orElse(null);
	}

	public Employee getEmployeeByName(String empName) {
		return empRepo.findbyName(empName);
	}
	
	public Employee addEmploee(Employee emp) {
		return empRepo.save(emp);
	}

	public Employee updateEmployee(Employee emp) {
		return empRepo.save(emp);
	}

	public void deleteEmployeeById(int empId) {
		empRepo.deleteById(empId);
	}

	public Employee empLogin(String emailId, String password) {
		return empRepo.empLogin(emailId, password);
	}
	
}
